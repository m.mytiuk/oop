package com.example.mytiuk.justcook.dimension;

/**
 * Created by Maksym Mytiuk
 */
public class Piece extends Dimension {
    public Piece() {
        super();
        name="штуки";
    }
    public Piece(double value) {
        super(value);
        name="штуки";
    }

    @Override
    protected double convertToBaseDimension(int index) {
        return value;
    }
}
