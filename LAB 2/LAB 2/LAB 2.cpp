﻿#define _CRT_SECURE_NO_WARNINGS
#define _USE_MATH_DEFINES
#include <iostream>
#include <stdio.h>
#include <cstdlib>
#include <string.h>
#include <math.h>

int main()
{
	setlocale(LC_ALL, "Russian");
	double a = 0.00000007, b = 180000000, c = 0.0000187, d = pow(5, 10) - 18, e = 0.1 * pow(10, -10), f = 1.0004 * 10 + 18;
	double g = 0.1e+6, h = 1.87e-18, i = 17e+3, j = 0.14e-8, k = 11e+4, l = 3e-14;
	printf("З плаваючою комою:\n");
	printf("%e\n", a);
	printf("%e\n", b);
	printf("%e\n", c);
	printf("%e\n", d);
	printf("%e\n", e);
	printf("%e\n\n", f);

	printf("В нормальному виглядi:\n");
	printf("%.10f\n", g);
	printf("%.10f\n", h);
	printf("%.10f\n", i);
	printf("%.10f\n", j);
	printf("%.10f\n", k);
	printf("%.14f\n", l);
	printf("\n");
	printf("\n");
	printf("\n");

	//Завдання 2
	double S;
	double x = -1.5;
	double y = 1.75 * pow(10, -3);
	double z = 0.805 * pow(10, 2);
	S = cbrt(3 + (x - y)) / (pow(x, 2) + pow(z, 3) + 4) - tan(z);
	printf("%.9f\n", S);
	printf("\n");
	printf("\n");
	printf("\n");

	//Завдання3
	int A = 15, B = 43, C = 23;
	printf("A = %d\nB = %d\nC = %d\n", A, B, C);
	A = A + B;
	B = A - B;
	A = A - B;
	B = B + C;
	C = B - C;
	B = B - C;
	printf("\nA = %d\nB = %d\nC = %d\n", A, B, C);
	printf("\n");
	printf("\n");
	printf("\n");

	//Завдання4
	float radians, degree;
	printf("Введiть радiани: ");
	scanf("%f", &radians);

	if ((0 < radians) && (radians < 2 * M_PI))
	{
		degree = (radians * 180) / M_PI;
		printf("Градуси: %f", degree);
	}
	else
	{
		printf("Введiть корректне значення радiан\n");
		return 0;
	}

	system("pause");
	return 0;
}

