#include <iostream>
#include "Vector.h"
using namespace std;

int main()
{
    Vector aVector(2, 3, 5, "First Vector");

    cout << aVector.getX() << '\n';
    cout << aVector.getY() << '\n';
    cout << aVector.getLength() << '\n';
    cout << aVector.getName() << '\n';

    Vector bVector(4, 7, 13, "Second Vector");
    Vector vectorSumm = aVector + bVector;
    cout << vectorSumm.getX() << '\n';
    cout << vectorSumm.getY() << '\n';
    cout << vectorSumm.getLength() << '\n';

    Vector dVector = aVector;
    cout << dVector.getName() << '\n';

    Vector cVector;
    cout << cVector.getX() << '\n';
    cout << cVector.getY() << '\n';
    cout << cVector.getLength() << '\n' ;
    cout << cVector.getName() << '\n';
    cVector.setX(5);

    cout << Vector::vectorCount;
}

