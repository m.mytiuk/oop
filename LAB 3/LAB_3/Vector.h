#pragma once
#include <math.h>
using namespace std;

class Vector {
private:
	int x, y; float length; string name = "";

public:
	static int vectorCount;
	Vector() {
		x = 0;
		y = 0;
		length = 0;
		vectorCount++; 
	};

	Vector(int a, int b, float length, string name = "") : x(a), y(b), length(length), name(name) {
		vectorCount++; 
	}

	Vector (const Vector & refVector)
	{
		x = refVector.x;
		y = refVector.y;
		length = refVector.length;
		name = refVector.name;
	}

	Vector operator + (const Vector&);

	void setX(int newX) {
		x = newX;
	}
	int getX() {
		return x;
	}

	void setY(int newY) {
		y = newY;
	}
	int getY() {
		return y;
	}

	void setLength(int newLength) {
		y = newLength;
	}
	int getLength() {
		if (!length) {
			length = sqrt(pow(x, 2) + pow(y, 2));
		}

		return length;
	}

	void setName(string name) {
		name = name;
	}
	string getName() {
		return name;
	}

};

int Vector::vectorCount = 0;

Vector Vector::operator+ (const Vector& param) {
	Vector temp;
	temp.x = x;
	temp.y = param.y;

	return temp;
}
